﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace yafskiGetNew
{
    public static class PositionBuilder
    {
        public static void Build (IList<IWebElement> positionData, List<StockPosition> positionDataList)
        {
            var symbolReg = new Regex("^\\D{1,5}(?=\\r\\n)");
            var currentPriceReg = new Regex("(?<=^\\D{1,5}\\r\\n)[\\d,]*(.)\\d{2,4}(?=\\r\\n)");
            var priceChangePercentReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n)[+-]?[\\d,]*(.)\\d{0,4}(?=%\\r\\n)");
            var priceChangeDollarReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n)[+-]?[\\d,]*(.)\\d{0,4}(?=\\r\\n)");
            var sharesReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?[\\d,]*(.)\\d{0,4}\\r\\n)[\\d,]*(.)\\d+(?=\\s)");
            var costBasisReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s)[\\d,]*(.)\\d{2,4}(?=\\r\\n)");
            var totalCurrentValueReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n)[\\d,]*(.)\\d{2,4}(?=\\r\\n)");
            var dayGainPercentReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n)[+-]?[\\d,]*(.)\\d{2,4}(?=%\\r\\n)");
            var dayGainDollarsReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n)[+-]?[\\d,]*(.)\\d{2,4}(?=\\r\\n)");
            var totalGainPercentReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n)[+-]?[\\d,]*(.)\\d{2,4}(?=%\\r\\n)");
            var totalGainDollarsReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n)[+-]?[\\d,]*(.)\\d{2,4}(?=\\r\\n)");
            var lotsReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n)[\\d,]*(?=\\slot)");
            var notesReg = new Regex("(?<=^\\D{1,5}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{0,4}%\\r\\n[+-]?\\d+(.)\\d{0,4}\\r\\n[\\d,]*(.)\\d+\\s[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n[+-]?[\\d,]*(.)\\d{2,4}%\\r\\n[+-]?[\\d,]*(.)\\d{2,4}\\r\\n[\\d,]*\\slot[s]*\\s).*");


            foreach (var item in positionData)
            {
                if (item.Text == "")
                    continue;

                var stockPosition = new StockPosition();
                var itemText = item.Text;

                var symbol = symbolReg.Match(itemText).ToString();
                var currentPrice = currentPriceReg.Match(itemText).ToString();
                var priceChangePercent = priceChangePercentReg.Match(itemText).ToString();
                var priceChangeDollars = priceChangeDollarReg.Match(itemText).ToString();
                var shares = sharesReg.Match(itemText).ToString();
                var costBasis = costBasisReg.Match(itemText).ToString();
                var totalCurrentValue = totalCurrentValueReg.Match(itemText).ToString();
                var dayGainPercent = dayGainPercentReg.Match(itemText).ToString();
                var dayGainDollars = dayGainDollarsReg.Match(itemText).ToString();
                var totalGainPercent = totalGainPercentReg.Match(itemText).ToString();
                var totalGainDollars = totalGainDollarsReg.Match(itemText).ToString();
                var lots = lotsReg.Match(itemText).ToString();
                var notes = notesReg.Match(itemText).ToString();

                stockPosition.Symbol = symbol;
                stockPosition.CurrentPrice = double.Parse(currentPrice, NumberStyles.Any);
                stockPosition.PriceChangeDollars = double.Parse(priceChangeDollars, NumberStyles.Any);
                stockPosition.PriceChangePercent = double.Parse(priceChangePercent, NumberStyles.Any);
                stockPosition.Shares = double.Parse(shares, NumberStyles.Any);
                stockPosition.CostBasis = double.Parse(costBasis, NumberStyles.Any);
                stockPosition.MarketValue = double.Parse(totalCurrentValue, NumberStyles.Any);
                stockPosition.DayGainPercent = double.Parse(dayGainPercent, NumberStyles.Any);
                stockPosition.DayGainDollars = double.Parse(dayGainDollars, NumberStyles.Any);
                stockPosition.TotalGainDollars = double.Parse(totalGainDollars, NumberStyles.Any);
                stockPosition.TotalGainPercent = double.Parse(totalGainPercent, NumberStyles.Any);
                stockPosition.LotCount = double.Parse(lots, NumberStyles.Any);
                stockPosition.Notes = notes;

                positionDataList.Add(stockPosition);
            }
        }

    }
}