﻿using System;
using System.Collections.Generic;

namespace yafskiGetNew
{
    public class PortfolioSnapshot
    {
        public int Id { get; set; }
        public DateTime AsOf { get; set; }
        public double CurrentMarketValue { get; set; }
        public double NetDayGainDollars { get; set; }
        public double NetDayGainPercent { get; set; }
        public double NetTotalGainDollars { get; set; }
        public double NetTotalGainPercent { get; set; }
        public virtual List<StockPosition> StockPositions { get; set; }
    }
}