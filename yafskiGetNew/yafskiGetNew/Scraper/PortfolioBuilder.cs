﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace yafskiGetNew
{
    public static class PortfolioBuilder
    {
        public static PortfolioSnapshot Build(IWebElement portfolio)
        {
            DateTime asOf = DateTime.Now;
            var portfolioSnapShot = new PortfolioSnapshot();

            var dollarRegEx = new Regex("[+-]?[\\S]+");
            var percentRegEx = new Regex("[+-]?\\d+.?\\d{0,4}(?=%)");

            var rawCurrentMarketValue = portfolio.FindElement(By.ClassName("_3wreg")).Text;
            Console.WriteLine();
            Console.WriteLine(rawCurrentMarketValue);
            Console.WriteLine();
            var rawDayGain = portfolio.FindElement(By.ClassName("_2ETlv")).FindElement(By.TagName("span")).Text;
            var rawTotalGain = portfolio.FindElement(By.ClassName("_2HvXW")).FindElement(By.TagName("span")).Text;

            var netDayGainDollars = dollarRegEx.Match(rawDayGain).ToString();
            var netDayGainPercent = percentRegEx.Match(rawDayGain).ToString();
            var netTotalGainDollars = dollarRegEx.Match(rawTotalGain).ToString();
            var netTotalGainPercent = percentRegEx.Match(rawTotalGain).ToString();

            portfolioSnapShot.AsOf = asOf;
            portfolioSnapShot.CurrentMarketValue = double.Parse(rawCurrentMarketValue, NumberStyles.Currency);
            portfolioSnapShot.NetDayGainDollars = double.Parse(netDayGainDollars, NumberStyles.Currency);
            portfolioSnapShot.NetDayGainPercent = double.Parse(netDayGainPercent, NumberStyles.Any);
            portfolioSnapShot.NetTotalGainDollars = double.Parse(netTotalGainDollars, NumberStyles.Currency);
            portfolioSnapShot.NetTotalGainPercent = double.Parse(netTotalGainPercent, NumberStyles.Any);

            IList<IWebElement> table = portfolio.FindElements(By.TagName("table"));
            var positionTable = table[1];

            IList<IWebElement> positionData = positionTable.FindElements(By.TagName("tbody"));

            List<StockPosition> positionDataList = new List<StockPosition>();

            PositionBuilder.Build(positionData, positionDataList);

            portfolioSnapShot.StockPositions = positionDataList;

            Console.WriteLine();
            Console.WriteLine(portfolioSnapShot.AsOf);
            Console.WriteLine("Current Market Value: ${0}", portfolioSnapShot.CurrentMarketValue);
            Console.WriteLine("Day Gain/Loss: ${0} | {1}%", portfolioSnapShot.NetDayGainDollars, portfolioSnapShot.NetDayGainPercent);
            Console.WriteLine("Total Gain/Loss: ${0} | {1}%", portfolioSnapShot.NetTotalGainDollars, portfolioSnapShot.NetTotalGainPercent);
            Console.WriteLine();
            foreach (var item in positionDataList)
            {
                Console.WriteLine("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10} | {11} | {12}", item.Symbol, item.CurrentPrice,
                    item.PriceChangePercent, item.PriceChangeDollars, item.Shares, item.CostBasis, item.MarketValue, item.DayGainPercent, item.DayGainDollars, item.TotalGainPercent,
                    item.TotalGainDollars, item.LotCount, item.Notes);
            }

            return portfolioSnapShot;
        }
    }
}