﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace yafskiGetNew
{
    public static class GetData
    {
        public static IWebElement Get(IWebDriver driver, string[] loginData)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

            driver.Navigate()
                .GoToUrl(
                    "https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=finance&.done=https%3A%2F%2Ffinance.yahoo.com%2F");

            IWebElement unField = driver.FindElement(By.Id("login-username"));
            unField.SendKeys(loginData[0]);
            unField.Submit();

            try
            {
                wait.Until(d => d.FindElement(By.Id("login-passwd")));
            }
            catch
            {
                throw new Exception("login");
            }

            IWebElement pwField = driver.FindElement(By.Id("login-passwd"));
            pwField.SendKeys(loginData[1]);
            
            IWebElement submitButton = driver.FindElement(By.Id("login-signin"));
            submitButton.Click();

            try
            {
                wait.Until(d => d.Url.StartsWith("https://finance.yahoo.com", StringComparison.OrdinalIgnoreCase));
            }
            catch
            {
                throw new Exception("login");
            }
            
            driver.Navigate().GoToUrl("https://finance.yahoo.com/portfolio/p_0/view/v2");

            wait.Until(d => d.FindElement(By.Id("main")));

            IWebElement portfolio = driver.FindElement(By.Id("main"));

            return portfolio;
        }
    }
}