using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace yafskiGetNew
{
    public static class GetNewSnapshot
    {
        [FunctionName("GetNewSnapshot")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var portfolioSnapShot = new PortfolioSnapshot();

            var loginData = new[]
            {
                System.Environment.GetEnvironmentVariable("YFUserName"),
                System.Environment.GetEnvironmentVariable("YFPass")
            };

            try
            {
                ChromeOptions option = new ChromeOptions();
                option.AddArgument("--headless");

                using (IWebDriver driver = new ChromeDriver(option))
                {
                    IWebElement portfolio = GetData.Get(driver, loginData);
                    portfolioSnapShot = PortfolioBuilder.Build(portfolio);
                }
            }
            catch (System.Exception e)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError, e);
            }

            log.Info("GetNewSnapshot HTTP trigger function processed a request.");

            return req.CreateResponse(HttpStatusCode.OK, portfolioSnapShot);

        }
    }
}
