﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace yafski
{
    public class PortfolioSnapshot
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime AsOf { get; set; }
        public double CurrentMarketValue { get; set; }
        public double NetDayGainDollars { get; set; }
        public double NetDayGainPercent { get; set; }
        public double NetTotalGainDollars { get; set; }
        public double NetTotalGainPercent { get; set; }
        public virtual List<StockPosition> StockPositions { get; set; }
    }
}