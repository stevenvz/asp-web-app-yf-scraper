﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace yafski.ViewModels
{
    public class SnapshotViewModel
    {
        public IEnumerable<StockPosition> StockPositions { get; set; }

        public int Id { get; set; }
        public DateTime AsOf { get; set; }
        public double CurrentMarketValue { get; set; }
        public double NetDayGainDollars { get; set; }
        public double NetDayGainPercent { get; set; }
        public double NetTotalGainDollars { get; set; }
        public double NetTotalGainPercent { get; set; }

        public SnapshotViewModel(PortfolioSnapshot portfolioSnapshot)
        {
            Id = portfolioSnapshot.Id;
            AsOf = portfolioSnapshot.AsOf;
            CurrentMarketValue = portfolioSnapshot.CurrentMarketValue;
            NetDayGainDollars = portfolioSnapshot.NetDayGainDollars;
            NetDayGainPercent = portfolioSnapshot.NetDayGainPercent;
            NetTotalGainDollars = portfolioSnapshot.NetTotalGainDollars;
            NetDayGainPercent = portfolioSnapshot.NetDayGainPercent;
        }
    }
}