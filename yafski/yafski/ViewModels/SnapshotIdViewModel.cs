﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace yafski.ViewModels
{
    public class SnapshotIdViewModel
    {
        public int Id { get; set; }

        public SnapshotIdViewModel(int id)
        {
            Id = id;
        }
    }
}