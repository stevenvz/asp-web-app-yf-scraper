﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace yafski.Dtos
{
    public class PortfolioSnapshotDto
    {
        public int Id { get; set; }
        public DateTime AsOf { get; set; }
        public double CurrentMarketValue { get; set; }
        public double NetDayGainDollars { get; set; }
        public double NetDayGainPercent { get; set; }
        public double NetTotalGainDollars { get; set; }
        public double NetTotalGainPercent { get; set; }
        public List<StockPositionDto> StockPositions { get; set; }
    }
}