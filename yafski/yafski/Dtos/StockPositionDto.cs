﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace yafski.Dtos
{
    public class StockPositionDto
    {
        public string Symbol { get; set; }
        public double CurrentPrice { get; set; }
        public double PriceChangeDollars { get; set; }
        public double PriceChangePercent { get; set; }
        public double Shares { get; set; }
        public double CostBasis { get; set; }
        public double MarketValue { get; set; }
        public double DayGainDollars { get; set; }
        public double DayGainPercent { get; set; }
        public double TotalGainDollars { get; set; }
        public double TotalGainPercent { get; set; }
        public double LotCount { get; set; }
        public string Notes { get; set; }

        public int Id { get; set; }
        public PortfolioSnapshotDto PortfolioSnapshot { get; set; }
    }
}