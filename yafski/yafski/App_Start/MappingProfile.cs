﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using yafski.Dtos;

namespace yafski.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<StockPosition, StockPositionDto>();
            Mapper.CreateMap<StockPositionDto, StockPosition>()
                .ForMember(c => c.Id, opt => opt.Ignore());

            Mapper.CreateMap<PortfolioSnapshot, PortfolioSnapshotDto>()
                .ForMember(dest => dest.StockPositions, opt => opt.MapFrom(src => src.StockPositions));
            Mapper.CreateMap<PortfolioSnapshotDto, PortfolioSnapshot>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            
        }
    }
}