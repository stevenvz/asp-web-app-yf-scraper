﻿using System;
using AutoMapper;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using yafski.Dtos;
using yafski.Models;

namespace yafski.Controllers.api
{
    public class NewSnapshotController : ApiController
    {
        private ApplicationDbContext _context;

        public NewSnapshotController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/newsnapshot
        public async Task<PortfolioSnapshotDto> GetSnapshots()
        {
            var portfolioSnapShot = new PortfolioSnapshot();
            
            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync("http://104.237.129.201/api/NewSnapshot");
            if (response.IsSuccessStatusCode)
            {
                portfolioSnapShot = await response.Content.ReadAsAsync<PortfolioSnapshot>();
            }

            _context.PortfolioSnapshots.Add(portfolioSnapShot);
            _context.SaveChanges();

            portfolioSnapShot.AsOf = DateTime.SpecifyKind(portfolioSnapShot.AsOf, DateTimeKind.Utc);

            var portfolioSnapShotDto = Mapper.Map<PortfolioSnapshot, PortfolioSnapshotDto>(portfolioSnapShot);
            
            return portfolioSnapShotDto;
        }
    }
}
