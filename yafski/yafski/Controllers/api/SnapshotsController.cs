﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using yafski.Dtos;
using yafski.Models;

namespace yafski.Controllers.api
{
    public class SnapshotsController : ApiController
    {
        private readonly ApplicationDbContext _context;

        public SnapshotsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/snapshots
        public IHttpActionResult GetSnapshots()
        {
            var snapshotsInDb = _context.PortfolioSnapshots.ToList();

            foreach (var item in snapshotsInDb)
            {
                item.AsOf = DateTime.SpecifyKind(item.AsOf, DateTimeKind.Utc);
            }

            var snapshotsDtos = snapshotsInDb
                .Select(Mapper.Map<PortfolioSnapshot, PortfolioSnapshotDto>);
            
            return Ok(snapshotsDtos);
        }

        // GET /api/snapshots/id
        public IHttpActionResult GetSnapshot(int id)
        {
            var snapshotInDb = _context.PortfolioSnapshots.SingleOrDefault(s => s.Id == id);

            if (snapshotInDb == null)
                return NotFound();

            snapshotInDb.AsOf = DateTime.SpecifyKind(snapshotInDb.AsOf, DateTimeKind.Utc);

            var snapshotDto = Mapper.Map<PortfolioSnapshot, PortfolioSnapshotDto>(snapshotInDb);

            return Ok(snapshotDto);
        }
    }
}
