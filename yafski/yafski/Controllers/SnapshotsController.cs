﻿using System.Web.Mvc;
using yafski.ViewModels;

namespace yafski.Controllers
{
    public class SnapshotsController : Controller
    {
        // GET: Snapshots
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            var snapshotIdModel = new SnapshotIdViewModel(id);

            return View("Snapshot", snapshotIdModel);
        }

        public ActionResult GetSnapshot()
        {
            return View("Index");
        }
    }
}