﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(yafski.Startup))]
namespace yafski
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
