namespace yafski.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8f1208c6-62cc-4475-875c-2c7be7d959c1', N'stevenvz@yafski.com', 0, N'ABtkkH0Ngyy8NYvr4wb4rXQhdegBHr+BXUFVP1b4bMHgl8U41hC79pnD7hdlM3Kpqg==', N'5610b9ba-d61a-4542-873d-a7dd633afac3', NULL, 0, 0, NULL, 1, 0, N'stevenvz@yafski.com')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'b910ae47-7265-41b4-9abe-7df3b91a36b7', N'HeadAdmin')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8f1208c6-62cc-4475-875c-2c7be7d959c1', N'b910ae47-7265-41b4-9abe-7df3b91a36b7')
            ");
        }
        
        public override void Down()
        {
        }
    }
}
