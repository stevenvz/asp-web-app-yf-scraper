namespace yafski.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustedPropNamesInModels2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.StockPositions", new[] { "PortfolioSnapshot_Id" });
            CreateIndex("dbo.StockPositions", "portfolioSnapshot_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.StockPositions", new[] { "portfolioSnapshot_Id" });
            CreateIndex("dbo.StockPositions", "PortfolioSnapshot_Id");
        }
    }
}
