namespace yafski.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PortfolioSnapshots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AsOf = c.DateTime(nullable: false),
                        CurrentMarketValue = c.Double(nullable: false),
                        NetDayGainDollars = c.Double(nullable: false),
                        NetDayGainPercent = c.Double(nullable: false),
                        NetTotalGainDollars = c.Double(nullable: false),
                        NetTotalGainPercent = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StockPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Symbol = c.String(),
                        CurrentPrice = c.Double(nullable: false),
                        PriceChangeDollars = c.Double(nullable: false),
                        PriceChangePercent = c.Double(nullable: false),
                        Shares = c.Double(nullable: false),
                        CostBasis = c.Double(nullable: false),
                        MarketValue = c.Double(nullable: false),
                        DayGainDollars = c.Double(nullable: false),
                        DayGainPercent = c.Double(nullable: false),
                        TotalGainDollars = c.Double(nullable: false),
                        TotalGainPercent = c.Double(nullable: false),
                        LotCount = c.Double(nullable: false),
                        Notes = c.String(),
                        PortfolioSnapshot_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PortfolioSnapshots", t => t.PortfolioSnapshot_Id)
                .Index(t => t.PortfolioSnapshot_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockPositions", "PortfolioSnapshot_Id", "dbo.PortfolioSnapshots");
            DropIndex("dbo.StockPositions", new[] { "PortfolioSnapshot_Id" });
            DropTable("dbo.StockPositions");
            DropTable("dbo.PortfolioSnapshots");
        }
    }
}
