# ASP-web-app-yf-scraper

This project is hosted @ www.yafski.com
(If you quickly want to use the site without creating an account, you can use
<br>username: guest@yafski.com
<br>password: Guest01!
<br>-there's no real-world financial data exposed here)

This application will log into a Yahoo! Finance portfolio that I created, scrape portfolio data, save it to a database, and display it in the web app.

When you are logged in, on the "Snapshots" page you'll see a table that contains an overview of prior scrapings. You can click on any of them to see the specifics of each position in the portfolio. Back on the "Snapshots" page, you can click the green "Get New Snapshots" button at the top to fetch/scrape a new set of up-to-date data.

For the actual code, the main solutions to look at are "yafski" & "yafskiCore".

<strong>"yafski"</strong> is the .NET Framework web application that is hosted by Microsoft Azure & makes a GET request to "yafskiCore" to do the actual scraping.

<strong>"yafskiCore"</strong> is the .NET Core application that is hosted on a linux-based virtual machine, and does the actual scraping.

An overview can be found on the "About" page of the site.

<strong>**The username & password for the mock Yahoo! Finance portfolio I created is exposed here in my code. I recognize that this is not good practice, and I would never do this in a real-world application. However, since there is no actual sensitive financial information contained anywhere in this project, and in order to keep costs low (the most secure method of storing sensitive data/keys I could find involved using a paid Microsoft service), I decided to simply leave the login info in my code. Again, I'd never actually do this in anything other than a demonstration application!**</strong>

A document that I put together to help the rest of the class with their projects can be found at:
https://docs.google.com/document/d/1kCM-Et36pXRwiUH-5LkAYIZAMZBN8_4461LonCb6NRc/edit?usp=sharing

Thank you for taking the time to look at my project!
<br>Steve Zarrella
<br>stevenvz@protonmail.com
