using System;
using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using Assert = NUnit.Framework.Assert;

namespace yafskiCore.UnitTestsCore
{
    [TestFixture]
    public class PositionBuilderTest
    {
        public IList<IWebElement> _positionData;
        public List<StockPosition> _positionDataList;

        [Test]
        public void PositionBuilderBuild_GivenValidInput_ShouldReturnMatchedAndParsedPositions()
        {
            // Setting up _positionData & _positionDataList by running Selenium multiple times & testing
            // individual Assert statements would take much longer, so I've handled the testing of RegEx matching & parsing
            // StockPosition data all in one unit test.

            _positionDataList = new List<StockPosition>();

            ChromeOptions option = new ChromeOptions();
            option.AddArgument("--headless");

            using (IWebDriver driver = new ChromeDriver(@"C:\Users\steve\source\repos\ASP-web-app-yf-scraper\yafskiCore\yafskiCore\bin\Debug\netcoreapp2.0", option))
            {
                driver.Navigate().GoToUrl(@"file:///C:/Users/steve/source/repos/ASP-web-app-yf-scraper/yafskiCore/data/MyWatchlist.html");

                IWebElement portfolio = driver.FindElement(By.Id("main"));

                IList<IWebElement> table = portfolio.FindElements(By.TagName("table"));
                var positionTable = table[1];

                _positionData = positionTable.FindElements(By.TagName("tbody"));

                PositionBuilder.Build(_positionData, _positionDataList);

                Assert.That(_positionDataList[0].Symbol == "GOOG");
                Assert.That(_positionDataList[0].CurrentPrice == 1137.51);
                Assert.That(_positionDataList[0].PriceChangeDollars == 7.72);
                Assert.That(_positionDataList[0].PriceChangePercent == 0.68);
                Assert.That(_positionDataList[0].Shares == 1000);
                Assert.That(_positionDataList[0].CostBasis == 950);
                Assert.That(_positionDataList[0].MarketValue == 1137510.00);
                Assert.That(_positionDataList[0].DayGainDollars == 7720.00);
                Assert.That(_positionDataList[0].DayGainPercent == 0.68);
                Assert.That(_positionDataList[0].TotalGainDollars == 187510.00);
                Assert.That(_positionDataList[0].TotalGainPercent == 19.74);
                Assert.That(_positionDataList[0].LotCount == 1);
                Assert.That(_positionDataList[0].Notes == "-");

                Assert.That(_positionDataList[4].Shares == 800);

                Assert.That(_positionDataList[6].CurrentPrice == 220.46);

                Assert.That(_positionDataList[10].Symbol == "AAPL");
            }
        }
    }
}
