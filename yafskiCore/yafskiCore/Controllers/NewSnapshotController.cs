﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using yafskiCore;

namespace yafskiCore.Controllers
{
    [Route("api/[controller]")]
    public class NewSnapshotController : Controller
    {
        // GET api/NewSnapshot
        [HttpGet]
        public PortfolioSnapshot Get()
        {
            var portfolioSnapShot = new PortfolioSnapshot();

            var loginData = new[]
            {
                "sts1789",
                "628@paveD"
            };

            ChromeOptions option = new ChromeOptions();
            option.AddArgument("--headless");

            using (IWebDriver driver = new ChromeDriver(@"C:\Users\steve\source\repos\ASP-web-app-yf-scraper\yafskiCore\yafskiCore\bin\Debug\netcoreapp2.0", option))
            {
                IWebElement portfolio = GetData.Get(driver, loginData);
                portfolioSnapShot = PortfolioBuilder.Build(portfolio);
            }
            
            return portfolioSnapShot;
        }

        /*// GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
